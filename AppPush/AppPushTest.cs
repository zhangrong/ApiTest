﻿using Able.Api.Tests.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;

namespace Able.Api.Tests.AppPush
{
    [TestClass]
    public class AppPushTest : BaseControllerTest
    {
        public AppPushTest()
        {
            url = "http://demo.comm01.com/AppMsgPush/";
        }
        [TestMethod]
        public void TestAddProgram()
        {
            url += "addapp.ashx";
            dict.Add("appName", "EaseSales");
            dict.Add("password", 123456);

            dict.Add("senderId", "4614510876"); //项目Id
            dict.Add("AuthorizationToken", "AIzaSyAkNtmjLRFpdu6WPJw8LrM4VI-9XCu2XzM");
            dict.Add("packageName", "com.able.easesales");

            url = string.Concat(url, "?", ApiHelper.BuildQuery(dict));


            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            string boundary = DateTime.Now.Ticks.ToString("x");
            request.ContentType = "multipart/form-data;boundary=" + boundary;
            request.Method = "POST";
            var path = @"D:\EaseSales\Able.Push\例子\证书.p12";
            byte[] datas = File.ReadAllBytes(path);

            //注意这里他在服务器端限制了文件格式，所以需要修改dataformat中的Content-Type属性
            #region
            //创建一个内存流
            Stream memStream = new MemoryStream();

            //确定上传的文件路径
            if (!String.IsNullOrEmpty(path))
            {
                boundary = "--" + boundary;

                //添加上传文件参数格式边界
                string paramFormat = boundary + "\r\nContent-Disposition: form-data; name=\"{0}\";\r\n\r\n{1}\r\n";
                NameValueCollection param = new NameValueCollection();
                param.Add("fname", Guid.NewGuid().ToString() + Path.GetExtension(path));

                //写上参数
                foreach (string key in param)
                {
                    string formitem = string.Format(paramFormat, key, param[key]);
                    byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                    memStream.Write(formitembytes, 0, formitembytes.Length);
                }

                //添加上传文件数据格式边界
                string dataFormat = boundary + "\r\nContent-Disposition: form-data; name=\"{0}\";filename=\"{1}\"\r\nContent-Type:application/x-pkcs12\r\n\r\n";
                string header = string.Format(dataFormat, "Filedata", Path.GetFileName(path));
                byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);
                memStream.Write(headerbytes, 0, headerbytes.Length);

                //获取文件内容
                FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);
                byte[] buffer = new byte[1024];
                int bytesRead = 0;

                //将文件内容写进内存流
                while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                {
                    memStream.Write(buffer, 0, bytesRead);
                }
                fileStream.Close();

                //添加文件结束边界
                byte[] boundarybytes = System.Text.Encoding.UTF8.GetBytes("\r\n\n" + boundary + "\r\nContent-Disposition: form-data; name=\"Upload\"\r\n\nSubmit Query\r\n" + boundary + "--");
                memStream.Write(boundarybytes, 0, boundarybytes.Length);

                //设置请求长度
                request.ContentLength = memStream.Length;
                //获取请求写入流
                Stream requestStream = request.GetRequestStream();


                //将内存流数据读取位置归零
                memStream.Position = 0;
                byte[] tempBuffer = new byte[memStream.Length];
                memStream.Read(tempBuffer, 0, tempBuffer.Length);
                memStream.Close();

                //将内存流中的buffer写入到请求写入流
                requestStream.Write(tempBuffer, 0, tempBuffer.Length);
                requestStream.Close();
            }
            #endregion

            var response = request.GetResponse() as HttpWebResponse;
            var stream = response.GetResponseStream();
            StreamReader reader = new StreamReader(stream);
            str = reader.ReadToEnd();
            var obj = JsonConvert.DeserializeObject<Result>(str);

            Assert.AreEqual(obj.Status, 1);
        }

        [TestMethod]
        public void TestSendMessage()
        {
            url += "addmessage.ashx";
            dict.Add("appName", "EaseSales");//唯一识别的东西
            dict.Add("aztokenList", "aztokenlist");
            dict.Add("iostokenlist", "iostokenlist");
            dict.Add("Message", "message");
            dict.Add("data", "data");

            str = ApiHelper.SendPost(url, dict);
            var obj = JsonConvert.DeserializeObject<Result>(str);

            Assert.AreEqual(obj.Status, 1);
            Console.WriteLine(str);
        }
    }
}
