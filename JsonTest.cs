﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace Able.Api.Tests
{
    [TestClass]
    public class JsonTest
    {
        [TestMethod]
        public void TestIgnoreNull()
        {
            var test = new Test
            {
                A = null,
                B = "121"
            };

            var str = JsonConvert.SerializeObject(test);
            Console.WriteLine(str);

            var test2 = JsonConvert.DeserializeObject(str);
            Console.WriteLine(JsonConvert.SerializeObject(test2));
        }

        [TestMethod]
        public void TestConvertNullToStringEmpty()
        {

        }
    }

    public class Test
    {
        public string A { get; set; }
        public string B { get; set; }
    }
}
