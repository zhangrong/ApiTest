﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Able.Api.Tests
{
    public enum HttpMethod
    {
        get,
        post,
    }
    public class ApiHelper
    {
        public const string Url = "http://api.easesales.com/easesales/api/";
        public const string Url2 = "http://localhost:1111/api/";

        public static string SendGet(string url, Dictionary<string, object> parameters = null, List<string> list = null)
        {
            string data = string.Empty;
            if (list != null)
            {
                foreach (var item in parameters)
                {
                    list.Add(item.Key + "_" + item.Value);
                }
                data = ApiHelper.BuildQuery(list, "utf-8");
            }
            else
            {
                data = ApiHelper.BuildQuery(parameters, "utf-8");
            }
            //创建请求
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url + "?" + data);

            //GET请求
            request.Method = "GET";
            request.ReadWriteTimeout = 5000000;
            request.ContentType = "text/html;charset=UTF-8";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream myResponseStream = response.GetResponseStream();
            StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("utf-8"));

            //返回内容
            string retString = myStreamReader.ReadToEnd();
            return retString;
        }
        /// <summary>
        /// Http (GET/POST)
        /// </summary>
        /// <param name="url">请求URL</param>
        /// <param name="parameters">请求参数</param>
        /// <param name="method">请求方法</param>
        /// <returns>响应内容</returns>
        /// <returns>响应内容</returns>
        public static string SendPost(string url, Dictionary<string, object> parameters, List<string> list = null)
        {
            var client = new WebClient();
            client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
            string data = string.Empty;

            if (list != null)
            {
                if (parameters != null)
                {
                    foreach (var item in parameters)
                    {
                        list.Add(item.Key + "_" + item.Value);
                    }
                }
                data = ApiHelper.BuildQuery(list, "utf-8");
            }
            else
            {
                if (parameters != null)
                {
                    data = ApiHelper.BuildQuery(parameters, "utf-8");
                }
            }
            var str = client.UploadString(url, "POST", data);
            return str;
        }
        public static string SendPost(string url, List<string> list = null)
        {
            return SendPost(url, null, list);
        }
        
        /// <summary>
        /// 组装普通文本请求参数。
        /// </summary>
        /// <param name="parameters">Key-Value形式请求参数字典</param>
        /// <returns>URL编码后的请求数据</returns>
        public static string BuildQuery(IDictionary<string, object> parameters, string encode = "utf-8")
        {
            if (parameters == null) return string.Empty;
            StringBuilder postData = new StringBuilder();
            bool hasParam = false;
            IEnumerator<KeyValuePair<string, object>> dem = parameters.GetEnumerator();
            while (dem.MoveNext())
            {
                string name = dem.Current.Key;
                string value = dem.Current.Value.ToString();
                // 忽略参数名或参数值为空的参数
                if (!string.IsNullOrEmpty(name))//&& !string.IsNullOrEmpty(value)
                {
                    if (hasParam)
                    {
                        postData.Append("&");
                    }
                    postData.Append(name);
                    postData.Append("=");
                    if (encode == "gb2312")
                    {
                        postData.Append(HttpUtility.UrlEncode(value, Encoding.GetEncoding("gb2312")));
                    }
                    else if (encode == "utf8")
                    {
                        postData.Append(HttpUtility.UrlEncode(value, Encoding.UTF8));
                    }
                    else
                    {
                        postData.Append(value);
                    }
                    hasParam = true;
                }
            }
            return postData.ToString();
        }
        public static string BuildQuery(List<string> list, string encode = "utf-8")
        {
            if (list == null) return string.Empty;
            StringBuilder postData = new StringBuilder();
            bool hasParam = false;

            foreach (var item in list)
            {
                var arr = item.Split('_');
                if (arr.Length == 2)
                {
                    string name = arr[0];
                    string value = arr[1];

                    // 忽略参数名或参数值为空的参数
                    if (!string.IsNullOrEmpty(name)) //&& !string.IsNullOrEmpty(value)
                    {
                        if (hasParam)
                        {
                            postData.Append("&");
                        }
                        postData.Append(name);
                        postData.Append("=");
                        if (encode == "gb2312")
                        {
                            postData.Append(HttpUtility.UrlEncode(value, Encoding.GetEncoding("gb2312")));
                        }
                        else if (encode == "utf8")
                        {
                            postData.Append(HttpUtility.UrlEncode(value, Encoding.UTF8));
                        }
                        else
                        {
                            postData.Append(value);
                        }
                        hasParam = true;
                    }
                }
            }
            return postData.ToString();
        }

        /// <summary>
        /// 把响应流转换为文本。
        /// </summary>
        /// <param name="rsp">响应流对象</param>
        /// <param name="encoding">编码方式</param>
        /// <returns>响应文本</returns>
        private static string GetResponseAsString(HttpWebResponse rsp, Encoding encoding)
        {
            System.IO.Stream stream = null;
            StreamReader reader = null;
            try
            {
                // 以字符流的方式读取HTTP响应
                stream = rsp.GetResponseStream();
                reader = new StreamReader(stream, encoding);
                return reader.ReadToEnd();
            }
            finally
            {
                // 释放资源
                if (reader != null) reader.Close();
                if (stream != null) stream.Close();
                if (rsp != null) rsp.Close();
            }
        }
    }

    public class Result
    {
        [JsonProperty("status")]
        public int Status { get; set; }
        [JsonProperty("msg")]
        public string Msg { get; set; }
        [JsonProperty("data")]
        public object Data { get; set; }
    }

}
