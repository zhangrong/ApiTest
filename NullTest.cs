﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Collections.Generic;

namespace Able.Api.Tests
{
    [TestClass]
    public class NullTest
    {
        [TestMethod]
        public void TestNull()
        {
            var test = new test();
            test = null;
            var flag = string.IsNullOrEmpty(test?.a);
            Console.WriteLine(flag);
        }

        public class test
        {
            public string a { get; set; }
        }

        [TestMethod]
        public void Test()
        {
            var list = new List<test>(0).Select(s => new
            {
                s.a
            });

            var a = new List<test>(0).Max(s => s.a);
            Console.WriteLine(a);
        }

        [TestMethod]
        public void Test1()
        {
            List<test> tests = new List<test>();
            Assert.AreEqual(true, tests as IEnumerable<object> != null);

            object[] objects = new object[10];

            Assert.AreEqual(true, objects as IEnumerable<object> != null);
        }

        [TestMethod]
        public void EnumTest()
        {
            Assert.AreEqual(Convert.ToInt32(1.00), 1);
            var num = 0.010000 * 100;

            //返回的"a"
            Assert.AreNotEqual("1", DeviceType.a);
        }
    }

    public enum DeviceType
    {
        a = '1',
        all = -1,
        android = 0,
        ios = 1,
    }
}
