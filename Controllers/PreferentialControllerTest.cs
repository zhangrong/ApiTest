﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Able.Api.Tests.Controllers
{
    [TestClass]
    public class PreferentialControllerTest : BaseControllerTest
    {
        public PreferentialControllerTest()
        {
            url = ApiHelper.Url + "Preferential/";
            dict.Add("siteId", 19);
            dict.Add("memberId", 1149);
            dict.Add("langId", 90);
        }
        [TestMethod]
        public void GetListUsed()
        {
            url += "getlist";

            dict.Add("pageindex", 1);
            dict.Add("pagesize", 10);
            dict.Add("type", 1);

            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }

        [TestMethod]
        public void GetListUnUsed()
        {
            url += "getlist";

            dict.Add("pageindex", 1);
            dict.Add("pagesize", 10);
            dict.Add("type", 2);

            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }

        [TestMethod]
        public void GetListExpired()
        {
            url += "getlist";

            dict.Add("pageindex", 1);
            dict.Add("pagesize", 10);
            dict.Add("type", 3);

            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }
    }
}
