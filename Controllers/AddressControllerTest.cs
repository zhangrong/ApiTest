﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;

namespace Able.Api.Tests.Controllers
{
    [TestClass]
    public class AddressControllerTest : BaseControllerTest
    {
        public AddressControllerTest()
        {
            url = ApiHelper.Url + "address/";
            dict.Add("siteId", 19);
            dict.Add("langId", 90);
        }

        [TestCategory("address")]
        [TestMethod]
        public void GetAddressList()
        {
            //獲取addresslist
            url += "GetList";
            dict.Add("memberId", 1151);

            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }
        [TestCategory("address")]
        [TestMethod]
        public void AddAddress()
        {
            //添加address(不需要addressid)
            url += "AddOrUpdate";
            dict.Add("memberId", 1149);
            dict.Add("address1", 1);
            dict.Add("address2", 2);
            dict.Add("address3", 3);
            dict.Add("address4", 4);
            dict.Add("addressdetail", "detail address");
            dict.Add("receivename", "zr");
            dict.Add("receivephone", "15875560234");
            dict.Add("isdefault", false);
            str = ApiHelper.SendPost(url, dict);
            RightAssert();


        }
        [TestCategory("address")]
        [TestMethod]
        public void EditAddress()
        {
            //修改address(需要addressid)
            url += "AddOrUpdate";
            dict.Add("memberId", 1149);
            dict.Add("addressid", 4440);
            dict.Add("address1", 1);
            dict.Add("address2", 2);
            dict.Add("address3", 3);
            dict.Add("address4", 4);
            dict.Add("addressdetail", "edit");
            dict.Add("receivename", "edit");
            dict.Add("receivephone", "15875560123");
            dict.Add("isdefault", true);
            str = ApiHelper.SendPost(url, dict);
            RightAssert();


        }
        [TestCategory("address")]
        [TestMethod]
        public void SetDefault()
        {
            url += "SetDefault";
            dict.Add("memberId", 1149);
            dict.Add("addressId", 4441);
            str = ApiHelper.SendPost(url, dict);
            RightAssert();


        }

        [TestCategory("address")]
        [TestMethod]
        public void DeleteList()
        {
            url += "DeleteList";
            dict.Add("memberId", 1151);
            //传递多个相同的参数
            list.Add("addressIds_13");
            list.Add("addressIds_14");


            //这样写是错的，只有对象才能使用这种写法
            //dict.Add("addressIds[0]", 4456);
            //dict.Add("addressIds[1]", 4456);


            str = ApiHelper.SendPost(url, dict, list);
            RightAssert();
        }


        [TestCategory("area")]
        [TestMethod]
        public void GetAreaList()
        {
            url += "GetAreaList";
            dict["siteId"] = 19;
            dict.Add("parentId", 0);
            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }

        [TestCategory("area")]
        [TestMethod]
        public void GetAreaAll()
        {
            url += "GetAreaAll";
            dict["siteId"] = 19;
            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }
    }
}
