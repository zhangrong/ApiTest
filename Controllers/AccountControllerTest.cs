﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Newtonsoft.Json;

namespace Able.Api.Tests.Controllers
{
    //结果 的标准输出:	远程服务器返回错误: (411) 所需的长度。

    [TestClass]
    public class AccountControllerTest : BaseControllerTest
    {
        public AccountControllerTest()
        {
            url = ApiHelper.Url2 + "account/";
            dict.Add("siteId", 19);
            dict.Add("langId", 90);
        }

        [TestCategory("account")]
        [TestMethod]
        public void Regisetr()
        {
            //url += "Register";
            //dict.Add("devicetype", 0);
            //dict.Add("email", "testRegister" + Guid.NewGuid().ToString().Substring(0, 20) + "@qq.com");
            //dict.Add("password", "123456");
            //dict.Add("phone", "15875560231" + new Random().Next(1000));
            //str = ApiHelper.SendPost(url, dict);
            //RightAssert();
        }
        [TestCategory("account")]
        [TestMethod]
        public void LoginEmail()
        {
            url += "login";
            dict.Add("devicetype", 0);
            dict.Add("email", "1184424167@qq.com");
            dict.Add("password", "123456");
            dict.Add("key", "");
            dict.Add("pushregisterid", "test_push_register_id_001");

            str = ApiHelper.SendPost(url, dict);
            RightAssert();
        }
        [TestCategory("account")]
        [TestMethod]
        public void LoginPhone()
        {
            url += "login";
            dict.Add("devicetype", 1);
            dict.Add("isUseGoogle", true);
            dict.Add("phone", "15875560234");
            dict.Add("password", "123456");
            dict.Add("pushregisterid", "1233442411");

            str = ApiHelper.SendPost(url, dict);
            RightAssert();
        }
        [TestMethod]
        public void CheckEmail()
        {
            url += "checkEmail";
            dict.Add("email", "1184424167@qq.com");

            str = ApiHelper.SendGet(url, dict);
            ErrorAssert(110101);
        }
        [TestMethod]
        public void CheckPhone()
        {
            url += "CheckPhone";
            dict.Add("phone", 15875560234);
            str = ApiHelper.SendGet(url, dict);
            //ErrorAssert(110102);
        }
        [TestCategory("account")]
        [TestMethod]
        public void ReSendActiveEmail()
        {
            url += "ReSendActiveEmail";
            dict.Add("email", "1184424167@qq.com");
            str = ApiHelper.SendPost(url, dict);
            RightAssert();


        }
        [TestCategory("account")]
        [TestMethod]
        public void ResetPwdEmail()
        {
            url += "ResetPwdEmail";
            dict.Add("email", "1184424167@qq.com");
            str = ApiHelper.SendPost(url, dict);
            RightAssert();
        }

        [TestMethod]
        public void RegisterThired()
        {
            url += "RegisterThired";
            dict.Add("thiredType", 1);
            dict.Add("deviceType", 0);
            dict.Add("thiredAppUserId", 1234255363);

            //可选
            dict.Add("Name", "Name");
            dict.Add("image", "image");

            dict.Add("pushRegisterId", "设备号");
            dict.Add("isUseGoogle", 1);

            str = ApiHelper.SendPost(url, dict);

            RightAssert();
        }
    }
}