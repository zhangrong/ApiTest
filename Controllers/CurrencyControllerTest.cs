﻿using System;
using System.Security.Policy;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Able.Api.Tests.Controllers
{
    [TestClass]
    public class CurrencyControllerTest : BaseControllerTest
    {
        public CurrencyControllerTest()
        {
            url = ApiHelper.Url + "Currency/";

        }
        [TestMethod]
        public void GetCurrency()
        {
            url += "GetCurrency";
            dict.Add("siteId", 19);
            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }
    }
}
