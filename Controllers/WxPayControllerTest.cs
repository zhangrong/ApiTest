﻿using Able.Api.Tests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;

namespace Able.Api.Tests.Controllers
{
    [TestClass]
    public class WxPayControllerTest : BaseControllerTest
    {
        string xml = "<xml><appid><![CDATA[wx6a95ac78858d4153]]></appid>\n<bank_type><![CDATA[CFT]]></bank_type>\n<cash_fee><![CDATA[1]]></cash_fee>\n<fee_type><![CDATA[CNY]]></fee_type>\n<is_subscribe><![CDATA[N]]></is_subscribe>\n<mch_id><![CDATA[1329925401]]></mch_id>\n<nonce_str><![CDATA[b4288d9c0ec0a1841b3b3728321e7088]]></nonce_str>\n<openid><![CDATA[o0Vd3vwaC4xBCkgUwsaTvza2jyOQ]]></openid>\n<out_trade_no><![CDATA[O160400049]]></out_trade_no>\n<result_code><![CDATA[SUCCESS]]></result_code>\n<return_code><![CDATA[SUCCESS]]></return_code>\n<sign><![CDATA[42B8861A0AFC801DF20A1EDFA97EE45F]]></sign>\n<time_end><![CDATA[20160415120304]]></time_end>\n<total_fee>1</total_fee>\n<trade_type><![CDATA[APP]]></trade_type>\n<transaction_id><![CDATA[4005282001201604154859538204]]></transaction_id>\n</xml>";
        public WxPayControllerTest()
        {
        }
        [TestMethod]
        public void Notify()
        {
            string url = ApiHelper.Url2 + "WxPay/Notify";
            //模拟微信的数据           

            var helper = new ByteArrHelper();
            byte[] bytes = helper.ToBytes(xml);
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            client.Headers[HttpRequestHeader.ContentType] = "application/xml";

            var str = client.UploadData(url, "POST", bytes);
        }

        [TestMethod]
        public void Notify2()
        {
            url = ApiHelper.Url2 + "WxPay/Notify";
            var helper = new ByteArrHelper();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xml);

            byte[] bytes = Encoding.UTF8.GetBytes(xmlDoc.OuterXml);

            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            //1.要写application/xml
            request.ContentType = "application/xml";

            request.ContentLength = bytes.Length;
            request.Method = "POST";

            var stream = request.GetRequestStream();

            stream.Write(bytes, 0, bytes.Length);

            var response = request.GetResponse();
            var stream2 = response.GetResponseStream();
            var str2 = new StreamReader(stream2).ReadToEnd();
            Console.WriteLine(str2);


        }
    }
}
