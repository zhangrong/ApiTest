﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Able.Api.Tests.Controllers
{
    [TestClass]
    public class EshopProductControllerTest : BaseControllerTest
    {
        public EshopProductControllerTest()
        {
            url += ApiHelper.Url + "EshopProduct/";
            dict.Add("siteId", 19);
            dict.Add("langId", 90);
        }

        [TestMethod]
        public void GetList()
        {
            url += "GetList";
            dict.Add("currencyId", "7");
            dict.Add("classId", "");
            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }

        [TestMethod]
        public void GetPagedList()
        {
            url += "GetPagedList";
            dict.Add("currencyId", "7");
            dict.Add("classId", "");
            dict.Add("pageIndex", "1");
            dict.Add("pageSize", "10");
            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }

        [TestMethod]
        public void SearchWithName()
        {
            //一定要找到的问题
            url += "GetList";
            dict.Add("currencyId", 7);
            dict.Add("productName", "三星");

            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }

        [TestMethod]
        public void GetDetail()
        {
            url += "GetDetail";
            dict.Add("currencyId", 7);
            dict.Add("productId", 47);
            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }

        [TestMethod]
        public void GetChildProducts()
        {
            //获取子产品的库存，价格，属性集合，子产品Id
            url += "GetChildProducts";
            //必要参数
            dict.Add("posProductId", 34099);//父产品Id
            dict.Add("currencyId", 7);

            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }

        [TestMethod]
        public void GetProperty()
        {
            url += "GetProperty";
            dict.Add("posProductId", 34099);

            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }

        [TestMethod]
        public void GetRecommandList()
        {
            url += "GetRecommandList";
            //必要参数
            dict.Add("currencyId", "7");//汇率
            dict.Add("count", 4); //首页显示的记录数

            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }

        [TestMethod]
        public void GetMoreRecommand()
        {
            url += "GetMoreRecommand";
            //必要参数
            dict.Add("currencyId", "7");//汇率
            dict.Add("cmId", 693); //区分多个推荐模块的Id

            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }

        [TestMethod]
        public void GetMorePagedRecommand()
        {
            url += "GetMorePagedRecommand";
            //必要参数
            dict.Add("currencyId", "7");//汇率
            dict.Add("cmId", 693); //区分多个推荐模块的Id
            dict.Add("pageIndex", 1);
            dict.Add("pageSize", 2);
            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }
    }
}
