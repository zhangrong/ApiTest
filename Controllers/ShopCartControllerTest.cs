﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Able.Api.Tests.Controllers
{
    [TestClass]
    public class ShopCartControllerTest : BaseControllerTest
    {
        public ShopCartControllerTest()
        {
            url += ApiHelper.Url2 + "ShopCart/";
            dict.Add("siteId", 19);
            dict.Add("langId", 90);
            dict.Add("memberId", 1149);
        }

        [TestMethod]
        public void GetList()
        {
            url += "GetList";
            dict.Add("currencyId", 7);
            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }

        [TestMethod]
        public void GetPagedList()
        {
            url += "GetPagedList";
            dict.Add("currencyId", 7);
            dict.Add("pageIndex", 1);
            dict.Add("pageSize", 10);

            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }
        [TestMethod]
        public void Serach()
        {
            url += "GetList";
            dict.Add("currencyId", 7);

            //查询传递参数
            dict.Add("productName", "三星");
            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }
        [TestMethod]
        public void Add()
        {
            //这里添加必须是添加真实的
            url += "Add";
            dict.Add("posChildProductId", 1);
            dict.Add("eshopProductId", 22);
            dict.Add("quantity", 2);

            str = ApiHelper.SendPost(url, dict);
            RightAssert();
        }
        [TestMethod]
        public void Update()
        {
            //可以修改商品数量和是否选中
            url += "Update";
            //必要参数
            dict.Add("posChildProductId", 22142);

            //可选需要修改的参数
            dict.Add("quantity", 10);//数量

            str = ApiHelper.SendPost(url, dict);
            //RightAssert();
        }

        [TestMethod]
        public void CheckAll()
        {
            //全选当前会员购物车中所有的数据
            url += "CheckAll";
            //必要参数
            dict.Add("isCheck", true);

            str = ApiHelper.SendPost(url, dict);
            RightAssert();
        }

        //没有用的测试
        [TestMethod]
        public void CheckAllNot()
        {
            url += "CheckAll";
            dict.Add("isCheck", false);

            str = ApiHelper.SendPost(url, dict);
            RightAssert();
        }
        [TestMethod]
        public void DeleteList()
        {
            //删除一到多个购物车中产品
            url += "DeleteList";
            //必要参数
            //这个参数和之前一样在服务器段也是一个数组
            dict.Add("posChildProductIds", 1);

            str = ApiHelper.SendPost(url, dict);
            RightAssert();
        }
        [TestMethod]
        public void Check()
        {
            url += "check";
            //dict.Add("items[0].posChildProductId", 330);
            //dict.Add("items[0].quantity", 20000000);
            //dict.Add("items[1].posChildProductId", 1);
            //dict.Add("items[1].quantity", 20);
            //dict.Add("items[2].posChildProductId", 1);
            //dict.Add("items[2].quantity", "0");

            List<CartItem> items = new List<CartItem>();
            items.Add(new CartItem
            {
                posChildProductId = 22145,
                quantity = 211,
            });
            
            var cartListStr = JsonConvert.SerializeObject(items);
            dict.Add("cartListStr", cartListStr);
            str = ApiHelper.SendPost(url, dict);
            RightAssert();
        }


        [TestMethod]
        public void GetPerson()
        {
            url += "GetPerson";

            dict.Add("[0].firstName", "zr");
            dict.Add("[0].lastName", "last");
            dict.Add("[1].firstName", "zr");
            dict.Add("[1].lastName", "last");
            dict.Add("[2].firstName", "zr");
            dict.Add("[2].lastName", "last");

            str = ApiHelper.SendPost(url, dict);
            RightAssert();
        }
    }

    public class CartItem
    {
        public int posChildProductId { get; set; }
        public uint quantity { get; set; }
    }
}
