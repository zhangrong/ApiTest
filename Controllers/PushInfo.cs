﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Able.Api.Tests.Controllers
{
    [TestClass]
    public class PushInfo : BaseControllerTest
    {
        public PushInfo()
        {
            url = ApiHelper.Url + "PushInfo/";
            dict.Add("siteId", 19);
            dict.Add("langId", 90);
        }

        [TestMethod]
        public void GetList()
        {
            url += "GetList";
            dict.Add("pageindex", 1);
            dict.Add("pageSize", 2);

            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }
    }
}
