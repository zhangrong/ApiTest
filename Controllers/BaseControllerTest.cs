﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using LitJson;

namespace Able.Api.Tests.Controllers
{
    public class BaseControllerTest
    {
        public string url = "", str = "";
        public Dictionary<string, object> dict = new Dictionary<string, object>();
        public List<string> list = new List<string>();

        public void RightAssert()
        {
            string str2 = str;
            var obj = JsonConvert.DeserializeObject<Result>(str);
            str = JsonConvert.SerializeObject(obj, Formatting.Indented);
            JsonData data = JsonMapper.ToObject(str);

            if (obj.Status != 100)
            {
                Console.WriteLine(new string('-', 50));
                Console.WriteLine(str);
            }
            Assert.AreEqual(obj.Status, 100);
            Console.WriteLine(str);
        }

        public void ErrorAssert(int status)
        {
            Console.WriteLine(str);
            var obj = JsonConvert.DeserializeObject<Result>(str);
            Assert.AreEqual(obj.Status, status);
            str = JsonConvert.SerializeObject(obj, Formatting.Indented);
            Console.WriteLine(str);
        }
    }
}
