﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System;
using Newtonsoft.Json;

namespace Able.Api.Tests.Controllers
{
    [TestClass]
    public class LanguageControllerTest : BaseControllerTest
    {
        public LanguageControllerTest()
        {
            url = ApiHelper.Url +"language/";
            dict.Add("siteId", 19);
        }
        [TestMethod]
        public void GetLanguage()
        {
            url += "getlanguage";
            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }
        [TestMethod]
        public void GetLanguage2()
        {
            url += "getlanguage";
            dict["siteId"] = "2a";
            str = ApiHelper.SendGet(url, dict);
            ErrorAssert(502);
        }
        [TestMethod]
        public void GetCommonIdentity()
        {
            url += "GetCommonIdentity";
            dict.Add("langId", 89);

            str = ApiHelper.SendGet(url, dict);
            RightAssert();


        }
    }
}
