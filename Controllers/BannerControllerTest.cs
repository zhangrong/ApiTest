﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Able.Api.Tests.Controllers
{
    [TestClass]
    public class BannerControllerTest : BaseControllerTest
    {
        public BannerControllerTest()
        {
            url = ApiHelper.Url + "Banner/";
            dict.Add("siteId", 19);
            dict.Add("langId", 90);
        }
        [TestMethod]
        public void GetList()
        {
            url += "getlist";

            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }


    }
}
