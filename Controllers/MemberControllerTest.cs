﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Able.Api.Tests.Controllers
{
    [TestClass]
    public class MemberControllerTest : BaseControllerTest
    {
        public MemberControllerTest()
        {
            url = ApiHelper.Url + "member/";
            dict.Add("siteId", 19);
            dict.Add("langId", 90);
        }

        [TestCategory("member")]
        [TestMethod]
        public void GetInfo()
        {
            url += "GetInfo";
            dict.Add("memberId", 1149);
            str = ApiHelper.SendGet(url, dict);
            RightAssert();


        }
        [TestCategory("member")]
        [TestMethod]
        public void ModifyInfo()
        {
            url += "ModifyInfo";
            dict.Add("memberId", 1149);
            dict.Add("memberName", "zrtest122" + new Random().Next(100));
            dict.Add("memberPhone", "testphone" + new Random().Next(100));
            str = ApiHelper.SendPost(url, dict);
            RightAssert();
        }
        [TestCategory("member")]
        [TestMethod]
        public void ChangePwd()
        {
            url += "ChangePwd";
            dict.Add("memberId", 1149);
            dict.Add("newPwd", 123456);
            dict.Add("oldPwd", 123456);
            str = ApiHelper.SendPost(url, dict);
            RightAssert();
        }
        [TestCategory("member")]
        [TestMethod]
        public void GetMemberPropertys()
        {
            url += "GetMemberPropertys";
            dict.Add("memberId", 1149);
            str = ApiHelper.SendGet(url, dict);
            RightAssert();


        }
        [TestCategory("member")]
        [TestMethod]
        public void UpdateMemberPropertys()
        {
            //我這裡會有亂碼的問題
            url += "UpdateMemberPropertys";
            dict.Add("memberId", "1149");
            dict.Add("f_32", "detailId_15,detailId_16");
            dict.Add("f_29", "detailId_12");
            dict.Add("f_30", "2011-11-1");
            dict.Add("f_28", @"管这样达到了我们要的效果，但是还是存在着问题：");
            dict.Add("f_27", "單行文本");
            str = ApiHelper.SendPost(url, dict);
            RightAssert();


        }
    }
}
