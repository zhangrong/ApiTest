﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Able.Api.Tests.Controllers
{
    [TestClass]
    public class ClassControllerTest : BaseControllerTest
    {
        public ClassControllerTest()
        {
            url = ApiHelper.Url +"class/";
            dict.Add("siteId", 19);
            dict.Add("langId", 90);
        }
        [TestMethod]
        public void GetListAll()
        {
            url += "GetListAll";
            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }
        [TestMethod]
        public void Get23List()
        {
            url += "Get23List";
            dict.Add("parentId", "10623");
            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }
        [TestMethod]
        public void GetListByParentId()
        {
            url += "GetListByParentId";
            dict.Add("parentId", "0");
            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }
    }
}
