﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Able.Api.Tests.Controllers
{
    [TestClass]
    public class MessageControllerTest : BaseControllerTest
    {
        public MessageControllerTest()
        {
            url = ApiHelper.Url + "message/";
        }

        public void Send()
        {
            dict.Add("siteId", 19);
            url += "Send";
            list.Add("messageIds_1");
            list.Add("messageIds_2");
            list.Add("messageIds_3");
            //list.Add("messageIds_4");
            //list.Add("messageIds_5");
            //list.Add("messageIds_6");

            str = ApiHelper.SendPost(url, dict, list);
            RightAssert();
        }

        public void GetMessageIds()
        {
            url += "GetMessageIds";

            dict.Add("siteId", 19);

            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }
    }
}
