﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Able.Api.Tests.Controllers
{
    [TestClass]
    public class OrderControllerTest : BaseControllerTest
    {
        public OrderControllerTest()
        {
            url += ApiHelper.Url + "order/";
            dict.Add("siteId", 19);
            dict.Add("langId", 90);
            dict.Add("memberId", 1149);
        }

        [TestMethod]
        public void GetList()
        {
            //获取订单集合
            url += "getlist";
            //可选参数
            dict.Add("status", "1"); //订单状态 
            //dict.Add("startDate", DateTime.Now.AddYears(-1)); //开始时间（订单下单时间）
            //dict.Add("endDate", DateTime.Now);   //结束时间（订单下单时间）
            //dict.Add("orderNo", "");//订单编号，唯一

            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }
        [TestMethod]
        public void GetPagedList()
        {
            //获取订单集合
            url += "getPagedlist";
            //可选参数
            dict.Add("status", "4"); //订单状态 
            dict.Add("startDate", DateTime.Now.AddYears(-1)); //开始时间（订单下单时间）
            dict.Add("endDate", DateTime.Now);   //结束时间（订单下单时间）
            dict.Add("orderNo", "");//订单编号，唯一
            dict.Add("pageIndex", 1);
            dict.Add("pageSize", 1);

            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }

        [TestMethod]
        public void GetDeatil()
        {
            //获取订单详情
            url += "Detail";

            //必填参数
            dict.Add("orderNo", "O160300004");

            //str = ApiHelper.SendGet(url, dict);
            //RightAssert();
        }

        [TestMethod]
        public void Cancel()
        {
            //取消订单
            url += "Cancel";
            //必填参数
            dict.Add("orderNo", "O160300028");

            str = ApiHelper.SendPost(url, dict);
            //RightAssert();
        }

        [TestMethod]
        public void UploadBankTrans()
        {
            //上传银行转账收据
            url += "UploadBankTrans";
            //必填参数
            dict.Add("orderNo", "O160300028");

            str = ApiHelper.SendPost(url, dict);
            //RightAssert();
        }

    }

    public class OrderStatus
    {
        //本来已下单时3，但是有可能没有设置邮费，所有是2

        /// <summary>
        /// 運費待定 2
        /// </summary>
        public const byte PendingShipMent = 2;

        /// <summary>
        /// 買家未付款 3
        /// </summary>
        public const byte UnPaid = 3;

        /// <summary>
        /// 交易取消 4
        /// </summary>
        public const byte TransactionCancelled = 4;

        /// <summary>
        /// 買家已付款,賣家待確認 5
        /// </summary>
        public const byte PaidWaitConfirmation = 5;

        /// <summary>
        /// 賣家已收款,待發貨 6
        /// </summary>
        public const byte ReceiveMoney = 6;

        /// <summary>
        /// 賣家已發貨,買家待收貨 7
        /// </summary>
        public const byte SenedProduct = 7;

        /// <summary>
        /// 交易完成 8
        /// </summary>
        public const byte TransactionComplete = 8;
    }
}
