﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Able.Api.Tests.Controllers
{
    [TestClass]
    public class CheckOutControllerTest : BaseControllerTest
    {
        public CheckOutControllerTest()
        {
            url = ApiHelper.Url2 + "CheckOut/";
            dict.Add("siteId", 19);
            dict.Add("langId", 90);
            dict.Add("currencyId", 7);
            dict.Add("memberId", 1149);
        }

        [TestMethod]
        public void GetPayType()
        {
            //不需要登录 获取支付方式
            url += "getPayType";

            str = ApiHelper.SendGet(url, dict);
            //RightAssert();
        }
        [TestMethod]
        public void GetSendType()
        {
            //不需要登录 获取送货方式
            url += "getSendType";

            str = ApiHelper.SendGet(url, dict);
            RightAssert();
        }
        [TestMethod]
        public void GetSendPrice()
        {
            //获取运费，切换地址后需要重新获取运费
            url += "GetSendPrice";
            //必须参数
            dict.Add("addressId", 4470);
            //list.Add("posChildProductIds_22145");
            //list.Add("posChildProductIds_303");
            //list.Add("posChildProductIds_359");

            str = ApiHelper.SendGet(url, dict, list);
        }

        [TestMethod]
        public void CheckCoupon()
        {
            //使用优惠券
            url += "CheckCoupon";
            dict.Add("coupon", "");

            str = ApiHelper.SendPost(url, dict);
            ErrorAssert(160401);
        }

        [TestMethod]
        public void CheckIntegral()
        {
            //使用积分
            url += "CheckIntegral";
            dict.Add("integral", "10000000");

            str = ApiHelper.SendPost(url, dict);
            ErrorAssert(160501);
        }

        [TestMethod]
        public void Save()
        {
            url += "Save";
            dict["memberId"] = 1149;
            //都是非必须
            dict.Add("addressId", "");
            dict.Add("ziquId", "1"); //自取方式的Id
            dict.Add("name", "zr"); //自取联系人姓名
            dict.Add("phone", "15875560234");//自取联系人phone
            dict.Add("remark", "");//给卖家的备注
            dict.Add("coupon", "");//优惠券
            dict.Add("integral", "");//积分

            str = ApiHelper.SendPost(url, dict);
            RightAssert();
        }
        [TestMethod]
        public void PrePay()
        {
            url += "PrePay";
            dict.Add("orderNo", "O160400049");
            str = ApiHelper.SendPost(url, dict);
            RightAssert();
        }
    }
}
