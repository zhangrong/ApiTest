﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Able.Api.Tests.Common
{
    /// <summary>
    /// 所有需要转化成byte[]的方法
    /// </summary>
    public class ByteArrHelper
    {
        public System.Drawing.Image ToImage(byte[] bytes)
        {
            MemoryStream ms = new MemoryStream(bytes);
            ms.Position = 0;
            Image img = Image.FromStream(ms);
            ms.Close();
            return img;
        }

        public string ToString(byte[] bytes)
        {
            System.Text.UTF8Encoding converter = new System.Text.UTF8Encoding();
            string inputString = converter.GetString(bytes);
            return inputString;
        }

        public byte[] ToBytes(string str)
        {
            UnicodeEncoding converter = new UnicodeEncoding();
            byte[] inputBytes = converter.GetBytes(str);
            return inputBytes;
        }

        public Stream ToStream(byte[] bytes)
        {
            Stream stream = new MemoryStream(bytes);
            return stream;
        }

        public byte[] ToBytes(Stream stream)
        {
            byte[] bytes = new byte[stream.Length];
            stream.Read(bytes, 0, bytes.Length);
            // 设置当前流的位置为流的开始
            stream.Seek(0, SeekOrigin.Begin);
            return bytes;
        }

        public byte[] ToBytes(Bitmap map)
        {
            //var map2 = new Bitmap("path");
            MemoryStream ms = new MemoryStream();
            map.Save(ms, System.Drawing.Imaging.ImageFormat.Icon);

            byte[] bytes = ms.GetBuffer();
            return bytes;
        }
    }
}
